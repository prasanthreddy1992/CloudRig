from . import (
    assign_bone_group,
    assign_bone_layers,
    bone_selection_pie_ops,
    edit_widget,
    flatten_chain,
    toggle_action_constraints,
    toggle_metarig,
)

modules = [
    assign_bone_group,
    assign_bone_layers,
    bone_selection_pie_ops,
    edit_widget,
    flatten_chain,
    toggle_action_constraints,
    toggle_metarig,
]
